package thumbtack;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created on 11.10.2015.
 */
public class Point2DTest {

    @Test
    public void testSetX() throws Exception {
        Point2D p = new Point2D();
        p.setX(-3.4);
    }

    @Test
    public void testMoveTo() throws Exception {
        Point2D m = new Point2D();
        m.moveTo(2323,2324);
    }

    @Test
    public void testEquals() {
        Point2D m = new Point2D(3,3);
        Point2D n = new Point2D(3,3);
        assertTrue(m.equals(n));
    }

    @Test
    public void testNotEquals() {
        Point2D m = new Point2D(3,3);
        Point2D n = new Point2D(2,2);
        assertFalse(m.equals(n));
    }

}