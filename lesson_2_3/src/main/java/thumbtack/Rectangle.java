package thumbtack;

/**
 * Created on 10.10.2015.
 *
 *
 * rectangle
 *
 * b-----------c
 * |           |
 * |           |
 * a-----------d
 */
class Rectangle{



    Point2D a,b,c,d;


    public Rectangle(Point2D a,Point2D b, Point2D c, Point2D d) {
        this.a = a;
        this.b=b;
        this.c=c;
        this.d=d;

    }

    public Rectangle(Point2D a,  Point2D c) {
        double x1=a.getX();
        double x2=c.getX();
        double y1=a.getY();
        double y2=c.getY();
        if(x2<x1){double t = x1;x1=x2;x2=t;}
        if(y2<y1){double t = y1;y1=y2;y2=t;}
        this.a = new Point2D(x1,y1);
        this.b=new Point2D(x1,y2);
        this.c=new Point2D(x2,y2);
        this.d=new Point2D(x2,y1);

    }

    public Rectangle(double h, double w){
        a = new Point2D(0,0);
        b= new Point2D(0,h);
        c= new Point2D(w,0);
        d=new Point2D(h,w);
    }
    public Rectangle(){
        a = new Point2D(0,0);
        b= new Point2D(0,1);
        c= new Point2D(1,0);
        d=new Point2D(1,1);
    }

    public void printPoints(){
        System.out.print("a (" + a.getX() + "," + a.getY() + ") \n" +
                "b (" + b.getX() + "," + b.getY() + ") \n" +
                "c (" + c.getX() + "," + c.getY() + ")\n" +
                "d (" + d.getX() + "," + d.getY() + ")\n");
    }

    public void move(double dx, double dy){
        a.setX(a.getX()+dx); a.setY(a.getY()+dy);
        b.setX(b.getX() + dx); b.setY(b.getY()+dy);
        c.setX(c.getX()+dx); c.setY(c.getY()+dy);
        d.setX(c.getX()+dx); c.setY(c.getY()+dy);
    }
    public void reduce(double nx,double ny){
        double newX=c.getX()/nx;
        double newY=c.getY()/ny;
        b.setY(newY);
        c.setY(newY);
        c.setX(newX);
        d.setX(newX);
    }

    public boolean inerhitXY(double x, double y)   //for a what?.. Ok...
    {
        return x>=a.getX() && x<=c.getY() && y>=a.getY() && y<=c.getY();
    }

    public boolean innerhit(Point2D n){
        double x = n.getX();
        double y = n.getY();
        return x>=a.getX() && x<=c.getY() && y>=a.getY() && y<=c.getY();
    }

    public double square(){
        return (c.getX()-a.getX())*(c.getY()-a.getY());
    }


/* TO DO
  public boolean Cross(Rectangle b){
        double x1 = b.a.getX();
        double y1 = b.a.getY();
        double x2 = b.c.getX();
        double y2 = b.c.getY();
        return (a.getX()<x2 && c.getX()>x2 && a.getY()<y2 && c.getY()>y1)
                || (a.getX()>x2 && c.getX()<x2 && a.getY()>y2 && c.getY()<y1);

    }*/



    public Point2D getA() {
        return a;
    }

    public Point2D getB() {
        return b;
    }

    public Point2D getC() {
        return c;
    }

    public Point2D getD() {
        return d;
    }

    public void setA(Point2D a) {
        this.a = a;
        this.b = new Point2D(a.getX(),b.getY());
        this.d = new Point2D(d.getX(),a.getY());
    }


    public void setC(Point2D c) {
        this.c = c;
        this.b=new Point2D(b.getX(),c.getY());
        this.d=new Point2D(c.getX(),d.getY());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rectangle)) return false;

        Rectangle rectangle = (Rectangle) o;

        if (!a.equals(rectangle.a)) return false;
        if (!b.equals(rectangle.b)) return false;
        if (!c.equals(rectangle.c)) return false;
        return d.equals(rectangle.d);

    }

    @Override
    public int hashCode() {
        int result = a.hashCode();
        result = 31 * result + b.hashCode();
        result = 31 * result + c.hashCode();
        result = 31 * result + d.hashCode();
        return result;
    }
}
