package thumbtack;

import thumbtack.exception.TraineeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

/**
 * size of area = 100*200
 */
public class App {
    public static void main(String[] args) {
        //
       /* double vol = new Rectangle3D(){
            double volume(){ return square3D()*h; }}.volume();


        System.out.println("Input count of squares:");
         Scanner sc = new Scanner(System.in);
        int c = sc.nextInt();
        List<Square> squares = new ArrayList<Square>();

        for (int i=0;i<c;i++){
            Point2D a = new Point2D(ThreadLocalRandom.current().nextDouble(0, 100),ThreadLocalRandom.current().nextDouble(0, 200));
            double side = ThreadLocalRandom.current().nextDouble(0, 20);
            System.out.print("("+a.getX()+ ","+a.getY()+","+side+")\n");
            Square s = new Square(a,side);
            squares.add(s);
        }

        double ss=0;
        int count = 0;
        for(int i=0;i<c;i++){
            for(int j=0;j<c;j++){
                ss+=squares.get(i).Innerhit(squares.get(j));
                if (squares.get(i).Innerhit(squares.get(j))>0) count++;
            }
        }
        System.out.print("SS="+ss+"\ncount="+count+"\n");
    }
*/

        try {
            Trainee one = new Trainee("Ivanov", null, 4);
        } catch (TraineeException e) {
            System.out.println(e.getMessage());
        }

        try {
            Trainee two = new Trainee("Petrov", "Petr", 25);
        } catch (TraineeException e) {
            System.out.println(e.getMessage());
        }

        try {
            Trainee three = new Trainee(null, "Ivan", 4);
        } catch (TraineeException e) {
            System.out.println(e.getMessage());
        }
    }
}


