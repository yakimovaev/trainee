package thumbtack.exception;

public enum TraineeErrorCodes {
    FIRST_NAME_EXCEPTION("first name exception message"),
    LAST_NAME_EXCEPTION("last name exception message"),
    GRADE_EXCEPTION("grage exception message");

    private String message;

    TraineeErrorCodes(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
