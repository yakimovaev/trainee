package thumbtack.exception;

/**
 * Created on 01.11.2015.
 */
public class TraineeException extends Exception {
    public TraineeException(TraineeErrorCodes errorCode) {
        super(errorCode.getMessage());
    }
}