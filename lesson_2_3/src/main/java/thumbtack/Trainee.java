package thumbtack;

import thumbtack.exception.TraineeException;

import static thumbtack.exception.TraineeErrorCodes.*;

/**
 * Created on 01.11.2015.
 */
public class Trainee {
    String firstName, lastName;
    int grade;

    public Trainee(String firstName, String lastName, int grade) throws TraineeException {
        setFirstName(firstName);
        setLastName(lastName);
        setGrade(grade);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getGrade() {
        return grade;
    }

    public void setFirstName(String firstName) throws TraineeException {
        if (firstName == null || "".equals(firstName)) {
            throw new TraineeException(FIRST_NAME_EXCEPTION);
        }

        this.firstName = firstName;
    }


    public void setLastName(String lastName) throws TraineeException {
        if (lastName == null || "".equals(lastName)) {
            throw new TraineeException(LAST_NAME_EXCEPTION);
        }

        this.lastName = lastName;
    }

    public void setGrade(int grade) throws TraineeException {
        if (grade >= 5 && grade <= 1) {
            throw new TraineeException(GRADE_EXCEPTION);
        }

        this.grade = grade;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trainee)) return false;

        Trainee trainee = (Trainee) o;

        if (grade != trainee.grade) return false;
        if (!firstName.equals(trainee.firstName)) return false;
        return lastName.equals(trainee.lastName);

    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + grade;
        return result;
    }
}
