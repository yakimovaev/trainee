package thumbtack;
/**
 * Created on 10.10.2015.
 */
class Point2D {

    private double x, y;

    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point2D() {
        x = 20;
        y = 30;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void moveTo(double newX, double newY) {
        x = newX;
        y = newY;
    }

    /*public static void main(String[] args) {

        Point2D point1 = new Point2D();
        Point2D point2 = new Point2D();
        if (point1.equals(point2))
            System.out.println("Points are equal");
        else
            System.out.println("Points are not equal");
    }*/


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Point2D other = (Point2D) obj;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        return true;
    }
}
