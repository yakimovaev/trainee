package thumbtack;

/**
 * Created on 18.10.2015.
 */
public class Rectangle3D extends Rectangle {
    double h;
   // Point2D a,b,c,d;
    Rectangle r;

    Rectangle3D(Point2D a,Point2D b, Point2D c, Point2D d, double h){
        this.r = new Rectangle(a,b,c,d);
        this.h=h;
    }

    Rectangle3D(double len, double wid,double h){
    this.h=h;
    r = new Rectangle(len,wid);

    }
    Rectangle3D(){
        h=1;
        r = new Rectangle();
    }

    protected void printPoints3D(){
        r.printPoints();
        System.out.println("height: " + h);
    }

    protected void reduce3D(double nx, double ny){
        r.reduce(nx,ny);
    }


    protected void move3D(double dx, double dy){
        r.move(dx, dy);
    }
    protected double square3D(){
       return r.square();

        //anon


    }

   // protected double volume(){
       // return square3D()*h;
    //}









    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    @Override
    public Point2D getA() {
        return a;
    }

    @Override
    public void setA(Point2D a) {
        this.a = a;
    }

    @Override
    public Point2D getB() {
        return b;
    }

    public void setB(Point2D b) {
        this.b = b;
    }

    @Override
    public Point2D getC() {
        return c;
    }

    @Override
    public void setC(Point2D c) {
        this.c = c;
    }

    @Override
    public Point2D getD() {
        return d;
    }

    public void setD(Point2D d) {
        this.d = d;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rectangle3D)) return false;
        if (!super.equals(o)) return false;

        Rectangle3D that = (Rectangle3D) o;

        if (Double.compare(that.h, h) != 0) return false;
        return !(r != null ? !r.equals(that.r) : that.r != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(h);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (r != null ? r.hashCode() : 0);
        return result;
    }
}
