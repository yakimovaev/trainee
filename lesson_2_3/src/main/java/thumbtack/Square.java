package thumbtack;

/**
 * Created on 11.10.2015.
 */
public class Square {
    Point2D a,b,c,d,center;



    public Square(Point2D a,  double side) {
        this.center=a;
        double half = side/2;
        this.a = new Point2D(a.getX()-half,a.getY()-half);
        this.b=new Point2D(a.getX()-half,side);
        this.c=new Point2D(side,side);
        this.d=new Point2D(side,a.getY()-half);
    }

    public double Innerhit(Square a1){
        double s = 0;
        if(c.getY()>a1.a.getY() && c.getX()>a1.a.getX() && a1.center.getX()>=center.getX() && a1.center.getY()>=center.getY()){
            s = (c.getY()-a1.a.getY()) * (c.getX()-a1.a.getX());
        }

        return s;
    }




    public Point2D getA() {
        return a;
    }

    public Point2D getB() {
        return b;
    }

    public Point2D getC() {
        return c;
    }

    public Point2D getD() {
        return d;
    }
}
